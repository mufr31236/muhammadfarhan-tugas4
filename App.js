import {View, Text} from 'react-native';
import React from 'react';
import Routing from './src/Routing';

const App = () => {
  return <Routing />;
};

export default App;
