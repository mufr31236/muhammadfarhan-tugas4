import {View, Text} from 'react-native';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Profile from './screen/Profile';
import EditProfil from './screen/EditProfil';
import FAQ from './screen/FAQ';

const Stack = createStackNavigator();

const ProfileNavigation = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="profile" component={Profile} />
      <Stack.Screen name="edit" component={EditProfil} />
      <Stack.Screen name="faq" component={FAQ} />
    </Stack.Navigator>
  );
};

export default ProfileNavigation;
