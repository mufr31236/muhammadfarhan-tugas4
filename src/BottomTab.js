import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';

const BottomTab = ({state, descriptors, navigation}) => {
  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        paddingTop: 5,
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        const getIcon = label => {
          switch (label) {
            case 'Home':
              return require('./assets/icon/home_ic.png');

            case 'Transaction':
              return require('./assets/icon/transaction_ic.png');

            case 'Profile':
              return require('./assets/icon/profile_ic.png');
          }
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{flex: 1, alignItems: 'center', paddingVertical: 10}}>
            <Image
              style={{tintColor: isFocused ? 'red' : '#D8D8D8'}}
              source={getIcon(label)}
            />
            <Text style={{color: isFocused ? 'red' : '#D8D8D8'}}>{label}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default BottomTab;
