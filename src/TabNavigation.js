import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import Home from './screen/Home';
import Keranjang from './screen/Keranjang';
import Profile from './screen/Profile';
import BottomTab from './BottomTab';
import HomeNavigation from './HomeNavigation';
import ProfileNavigation from './ProfileNavigation';
import TransactionNavigation from './TransactionNavigation';

const Tabs = createBottomTabNavigator();

const TabNavigation = () => {
  return (
    <Tabs.Navigator
      screenOptions={{
        headerShown: false,
      }}
      tabBar={prop => <BottomTab {...prop} />}>
      <Tabs.Screen name="Home" component={HomeNavigation} />
      <Tabs.Screen name="Transaction" component={TransactionNavigation} />
      <Tabs.Screen name="Profile" component={ProfileNavigation} />
    </Tabs.Navigator>
  );
};

export default TabNavigation;
