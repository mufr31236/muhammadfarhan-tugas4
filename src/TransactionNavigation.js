import {View, Text} from 'react-native';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Transaksi from './screen/Transaksi';
import TransaksiDetail from './screen/TransaksiDetail';
import Checkout from './screen/Checkout';

const Stack = createStackNavigator();

const TransactionNavigation = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="transaction" component={Transaksi} />
      <Stack.Screen name="detail" component={TransaksiDetail} />
      <Stack.Screen name="checkout" component={Checkout} />
    </Stack.Navigator>
  );
};

export default TransactionNavigation;
