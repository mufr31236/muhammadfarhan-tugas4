import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView,
  FlatList,
} from 'react-native';
import React, {useState} from 'react';

const Checkout = ({navigation, route}) => {
  const [payment, setPayment] = useState([
    {
      method: 'Bank Transfer',
      choosen: false,
      image: require('../assets/image/transfer_img.png'),
    },
    {
      method: 'Ovo',
      choosen: false,
      image: require('../assets/image/ovo_img.png'),
    },
    {
      method: 'Kartu Kredit',
      choosen: false,
      image: require('../assets/image/credit_img.png'),
    },
  ]);

  const handleItemChoosen = item => {
    console.log('item  : ', item);
    const updateDate = payment.map(x =>
      x.method === item.method
        ? {...x, choosen: !x.choosen}
        : {...x, choosen: false},
    );
    console.log('1111111111', updateDate);
    setPayment(updateDate);
  };

  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 15,
            paddingVertical: 15,
            shadowOpacity: 1,
            shadowRadius: 4,
            shadowColor: '#DCDCDC40',
            elevation: 4,
            marginBottom: 5,
            backgroundColor: '#FFFFFF',
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              style={{marginRight: 10}}
              source={require('../assets/icon/black_back_arrow_ic.png')}
            />
          </TouchableOpacity>

          <Text
            style={{
              color: '#201F26',
              fontSize: 18,
              fontWeight: '700',
              tintColor: 'black',
            }}>
            Summary
          </Text>
        </View>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            paddingVertical: 10,
            paddingHorizontal: 20,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginVertical: 5,
            }}>
            Data Customer
          </Text>
          <Text style={{color: '#201F26', marginVertical: 5}}>
            Agil Bani (0813763476)
          </Text>
          <Text style={{color: '#201F26', marginVertical: 5}}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={{color: '#201F26', marginVertical: 5}}>
            gantengdoang@dipanggang.com
          </Text>
        </View>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            paddingVertical: 10,
            paddingHorizontal: 20,
            marginTop: 10,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginVertical: 5,
            }}>
            Alamat Outlet Tujuan
          </Text>
          <Text style={{color: '#201F26', marginVertical: 5}}>
            Jack Repair - Seturan (027-343457)
          </Text>
          <Text style={{color: '#201F26', marginVertical: 5}}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            paddingVertical: 10,
            paddingHorizontal: 20,
            marginTop: 10,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginVertical: 5,
            }}>
            Barang
          </Text>
          <View
            style={{
              backgroundColor: 'white',
              flexDirection: 'row',
              borderRadius: 10,
            }}>
            <Image
              style={{height: 84, width: 84, marginRight: 10}}
              source={require('../assets/image/product_img.png')}
            />
            <View style={{marginLeft: 10}}>
              <Text
                style={{
                  color: '#000000',
                  fontSize: 12,
                  fontWeight: '500',
                  marginVertical: 5,
                }}>
                New Balance - Pink Abu - 40
              </Text>
              <Text
                style={{
                  color: '#737373',
                  fontSize: 12,
                  fontWeight: '500',
                  marginVertical: 5,
                }}>
                Cuci Sepatu
              </Text>
              <Text
                style={{
                  color: '#737373',
                  fontSize: 12,
                  fontWeight: '500',
                  marginVertical: 5,
                }}>
                Note : -
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 10,
            }}>
            <Text style={{color: '#000000', fontSize: 16, fontWeight: '400'}}>
              1 Pasang
            </Text>
            <Text style={{color: '#000000', fontSize: 16, fontWeight: '700'}}>
              Rp.50.000
            </Text>
          </View>
        </View>

        <View
          style={{
            backgroundColor: '#FFFFFF',
            paddingVertical: 10,
            paddingHorizontal: 20,
            marginTop: 10,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginVertical: 5,
            }}>
            Rincian Pembayaran
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 10,
            }}>
            <Text style={{color: '#000000', fontSize: 12, fontWeight: '400'}}>
              Cuci Sepatu{'   '}
              <Text style={{color: '#FFC107', fontSize: 12, fontWeight: '500'}}>
                x 1 Pasang
              </Text>
            </Text>
            <Text style={{color: '#000000', fontSize: 12, fontWeight: '400'}}>
              Rp.30 000
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 10,
            }}>
            <Text style={{color: '#000000', fontSize: 12, fontWeight: '400'}}>
              Biaya Antar
            </Text>
            <Text style={{color: '#000000', fontSize: 12, fontWeight: '400'}}>
              Rp.3 000
            </Text>
          </View>
          <View
            style={{
              borderBottomColor: 'black',
              borderBottomWidth: StyleSheet.hairlineWidth,
              marginVertical: 5,
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 10,
            }}>
            <Text style={{color: '#000000', fontSize: 12, fontWeight: '400'}}>
              Total
            </Text>
            <Text style={{color: '#034262', fontSize: 12, fontWeight: '700'}}>
              Rp.33 000
            </Text>
          </View>
        </View>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            paddingVertical: 10,
            paddingHorizontal: 20,
            marginTop: 10,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginVertical: 5,
            }}>
            Pilih Pembayaran
          </Text>

          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            data={payment}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() => {
                  handleItemChoosen(item);
                }}>
                <View
                  style={{
                    borderRadius: 10,
                    alignItems: 'center',
                    borderColor: item?.choosen ? '#034262' : '#E1E1E1',
                    borderWidth: 1,
                    backgroundColor: item?.choosen
                      ? 'rgba(3, 66, 98, 0.16)'
                      : 'white',
                    height: 82,
                    width: 126,
                    paddingVertical: 15,
                    marginHorizontal: 10,
                  }}>
                  {item.choosen ? (
                    <Image
                      style={{position: 'absolute', right: 5, top: 5}}
                      source={require('../assets/icon/choosen_ic.png')}
                    />
                  ) : null}
                  <Image source={item.image} />
                  <Text
                    style={{
                      color: '#000000',
                      fontSize: 12,
                      fontWeight: '400',
                    }}>
                    {item.method}
                  </Text>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
        <View style={{padding: 10, backgroundColor: 'white'}}>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              borderRadius: 10,
              backgroundColor: '#BB2427',
              paddingVertical: 20,
            }}>
            <Text style={{color: '#FFFFFF', fontWeight: '700', fontSize: 16}}>
              Pesan Sekarang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Checkout;
