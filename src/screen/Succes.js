import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';

const Succes = ({navigation, route}) => {
  return (
    <View style={{flex: 1}}>
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}>
        <Image
          style={{marginLeft: 10, marginTop: 10}}
          source={require('../assets/icon/close_ic.png')}
        />
      </TouchableOpacity>
      <Text
        style={{
          color: '#11A84E',
          fontSize: 18,
          fontWeight: '700',
          marginTop: 150,
          alignSelf: 'center',
        }}>
        Reservasi Berhasil
      </Text>
      <View
        style={{
          height: 160,
          width: 160,
          borderRadius: 10,
          backgroundColor: 'rgba(17, 168, 78, 0.4)',
          alignSelf: 'center',
          marginTop: 30,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Image
          style={{width: 60, height: 44}}
          source={require('../assets/image/check_img.png')}
        />
      </View>
      <Text
        style={{
          color: '#000000',
          fontSize: 18,
          fontWeight: '400',
          marginHorizontal: 20,
          marginVertical: 50,
          textAlign: 'center',
        }}>
        Kami Telah Mengirimkan Kode Reservasi ke Menu Transaksi
      </Text>

      <TouchableOpacity
        onPress={() => {
          navigation.navigate('tabnavigation', {screen: 'Transaction'});
        }}
        style={{
          position: 'absolute',
          bottom: 45,
          backgroundColor: '#BB2427',
          alignSelf: 'center',
          width: '90%',
          padding: 15,
          alignItems: 'center',
          marginHorizontal: 10,
          borderRadius: 10,
        }}>
        <Text style={{fontSize: 16, fontWeight: '700', color: '#FFFFFF'}}>
          Reservasi Sekarang
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Succes;
