import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
  Animated,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';

const FAQ = ({navigation, route}) => {
  const [question, setQuestion] = useState([
    {
      id: 1,
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isOpen: false,
    },
    {
      id: 2,
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isOpen: false,
    },
    {
      id: 3,
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isOpen: false,
    },
    {
      id: 4,
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isOpen: false,
    },
    {
      id: 5,
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isOpen: false,
    },
    {
      id: 6,
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
      isOpen: false,
    },
  ]);

  let rotateValueHolder = useRef(new Animated.Value(0)).current;

  const rotateData = rotateValueHolder.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '180deg'],
  });

  const rotateChevron = () => {
    Animated.timing(rotateValueHolder, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  };

  const handleItemClick = item => {
    const updateState = question.map(x =>
      x.id === item.id ? {...x, isOpen: !x.isOpen} : {...x, isOpen: false},
    );
    setQuestion(updateState);
  };
  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 15,
          paddingVertical: 15,
          shadowOpacity: 1,
          shadowRadius: 4,
          shadowColor: '#DCDCDC40',
          elevation: 4,
          marginBottom: 5,
          backgroundColor: '#FFFFFF',
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            style={{marginRight: 10}}
            source={require('../assets/icon/black_back_arrow_ic.png')}
          />
        </TouchableOpacity>

        <Text
          style={{
            color: '#201F26',
            fontSize: 18,
            fontWeight: '700',
            tintColor: 'black',
          }}>
          FAQ
        </Text>
      </View>
      <FlatList
        data={question}
        renderItem={({item}) => (
          <View
            style={{
              backgroundColor: 'white',
              marginHorizontal: 5,
              marginVertical: 10,
              padding: 15,
              borderRadius: 10,
              flexDirection: 'row',
            }}>
            <View style={{width: '90%'}}>
              <Text style={{color: '#000000', fontWeight: '500', fontSize: 14}}>
                {item.title}
              </Text>
              {item.isOpen ? (
                <Text
                  style={{color: '#595959', fontWeight: '500', fontSize: 14}}>
                  {item.title}
                </Text>
              ) : null}
            </View>
            <TouchableOpacity
              onPress={() => {
                rotateChevron();
                handleItemClick(item);
              }}>
              {/* <Animated.Image
                style={{transform: rotateData}}
                source={require('../assets/icon/chevron_ic.png')}
              /> */}
              {item.isOpen ? (
                <Image source={require('../assets/icon/chevron_ic.png')} />
              ) : (
                <FadeInView>
                  <Image source={require('../assets/icon/chevron_ic.png')} />
                </FadeInView>
              )}
            </TouchableOpacity>
          </View>
        )}
      />
    </View>
  );
};

const FadeInView = props => {
  const fadeAnim = useRef(new Animated.Value(0)).current; // Initial value for opacity: 0

  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }, [fadeAnim]);
  const rotateData = fadeAnim.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '180deg'],
  });

  return (
    <Animated.View // Special animatable View
      style={{
        ...props.style,
        transform: [{rotate: rotateData}], // Bind opacity to animated value
      }}>
      {props.children}
    </Animated.View>
  );
};

export default FAQ;

{
  /* <Animated.View // Special animatable View
style={{
  transform: [{rotate: rotateData}], // Bind opacity to animated value
}}>
<Text style={{fontSize: 28, textAlign: 'center', margin: 10}}>
  Fading in
</Text>
</Animated.View> */
}
{
  /* <Image
                  style={{transform: [{rotate: '180deg'}]}}
                  source={require('../assets/icon/chevron_ic.png')}
                /> */
}
