import {View, Text, StyleSheet, Image, ScrollView} from 'react-native';
import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';

const Home = ({navigation, route}) => {
  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: 'white',
        flex: 1,
      }}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 50,
            width: '100%',
            justifyContent: 'space-between',
            backgroundColor: 'white',
            paddingHorizontal: 20,
          }}>
          <View
            style={{
              flexDirection: 'column',
              marginHorizontal: 20,
            }}>
            <Image
              source={require('../assets/icon/profile_pic_ic.png')}
              style={{height: 45, width: 45}}
            />
            <Text style={{fontSize: 15, color: '#034262', marginTop: 10}}>
              Hello,Agil!
            </Text>
          </View>
          <Image
            source={require('../assets/icon/bag_bold_ic.png')}
            style={{height: 24, width: 24}}
          />
        </View>
        <Text
          style={{
            fontSize: 20,
            fontWeight: '700',
            marginVertical: 10,
            marginHorizontal: 20,
          }}>
          Ingin merawat dan perbaiki {'\n'}sepatumu? cari disini
        </Text>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 10,
            width: '100%',
            justifyContent: 'space-between',
            marginVertical: 10,
          }}>
          <View
            style={{
              width: '80%',
              backgroundColor: '#F6F8FF',
              borderRadius: 10,
              height: 45,
              marginHorizontal: 10,
            }}>
            <Image
              style={{marginTop: 10, marginHorizontal: 10}}
              source={require('../assets/icon/search_ic.png')}
            />
          </View>
          <View
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 10,
              height: 45,
            }}>
            <Image
              style={{marginTop: 10, marginHorizontal: 10}}
              source={require('../assets/icon/filter_ic.png')}
            />
          </View>
        </View>
        <View style={{flexDirection: 'column', backgroundColor: '#F6F8FF'}}>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 30,
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                flexDirection: 'column',
                height: 95,
                width: 95,
                backgroundColor: 'white',
                borderRadius: 10,
                alignItems: 'center',
              }}>
              <Image
                style={{marginTop: 15}}
                source={require('../assets/icon/shoe_ic.png')}
              />
              <Text style={{color: '#BB2427', alignSelf: 'center'}}>
                Sepatu
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'column',
                height: 95,
                width: 95,
                backgroundColor: 'white',
                borderRadius: 10,
                alignItems: 'center',
              }}>
              <Image
                style={{marginTop: 15}}
                source={require('../assets/icon/jacket_ic.png')}
              />
              <Text style={{color: '#BB2427', alignSelf: 'center'}}>Jaket</Text>
            </View>
            <View
              style={{
                flexDirection: 'column',
                height: 95,
                width: 95,
                backgroundColor: 'white',
                borderRadius: 10,
                alignItems: 'center',
              }}>
              <Image
                style={{marginTop: 15}}
                source={require('../assets/icon/bag_ic.png')}
              />
              <Text style={{color: '#BB2427', alignSelf: 'center'}}>Tas</Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 30,
              marginVertical: 10,
              justifyContent: 'space-between',
            }}>
            <Text style={{fontSize: 12, fontWeight: '600', color: '#0A0827'}}>
              Rekomendasi Terdekat
            </Text>
            <Text style={{fontSize: 10, fontWeight: '500', color: '#E64C3C'}}>
              View All
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 30,
              marginVertical: 10,
              height: 133,
              backgroundColor: 'white',
              borderRadius: 10,
            }}>
            <Image
              source={require('../assets/image/shop1_img.png')}
              style={{
                borderRadius: 10,
                height: 121,
                marginVertical: 5,
                marginHorizontal: 5,
              }}
            />
            <View
              style={{
                flexDirection: 'column',
                marginVertical: 5,
                width: '70%',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    width: '80%',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Image
                      source={require('../assets/icon/star_ic.png')}
                      style={{height: 8, width: 7}}
                    />
                    <Image
                      source={require('../assets/icon/star_ic.png')}
                      style={{height: 8, width: 7}}
                    />
                    <Image
                      source={require('../assets/icon/star_ic.png')}
                      style={{height: 8, width: 7}}
                    />
                    <Image
                      source={require('../assets/icon/star_ic.png')}
                      style={{height: 8, width: 7}}
                    />
                    <Image
                      source={require('../assets/icon/empty_star_ic.png')}
                      style={{height: 8, width: 7}}
                    />
                  </View>
                  <Text
                    style={{color: '#D8D8D8', fontSize: 10, fontWeight: '500'}}>
                    4.8 ratings
                  </Text>
                </View>
                <View
                  style={{
                    width: '20%',
                    alignItems: 'flex-end',
                  }}>
                  <Image
                    source={require('../assets/icon/red_heart_ic.png')}
                    style={{height: 13, width: 12}}
                  />
                </View>
              </View>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 12,
                  fontWeight: '600',
                  marginVertical: 5,
                }}>
                Jack Repair Gejayan
              </Text>
              <Text
                style={{color: '#D8D8D8', fontSize: 9, fontWeight: '500'}}
                numberOfLines={1}>
                Jl. Gejayan III No.2, Karangasem, Kec. Lawengan...
              </Text>
              <View
                style={{
                  borderRadius: 20,
                  width: 60,
                  backgroundColor: '#E64C3C33',
                  alignItems: 'center',
                  marginVertical: 5,
                }}>
                <Text
                  style={{
                    color: '#EA3D3D',
                    paddingHorizontal: 5,
                    paddingVertical: 5,
                    fontSize: 12,
                  }}>
                  Tutup
                </Text>
              </View>
            </View>
          </View>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('homenavigation', {screen: 'detail'})
            }>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 30,
                marginVertical: 10,
                height: 133,
                backgroundColor: 'white',
                borderRadius: 10,
              }}>
              <Image
                source={require('../assets/image/shop2_img.png')}
                style={{
                  borderRadius: 10,
                  height: 121,
                  marginVertical: 5,
                  marginHorizontal: 5,
                }}
              />
              <View
                style={{
                  flexDirection: 'column',
                  marginVertical: 5,
                  width: '70%',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flexDirection: 'column',
                      width: '80%',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={require('../assets/icon/star_ic.png')}
                        style={{height: 8, width: 7}}
                      />
                      <Image
                        source={require('../assets/icon/star_ic.png')}
                        style={{height: 8, width: 7}}
                      />
                      <Image
                        source={require('../assets/icon/star_ic.png')}
                        style={{height: 8, width: 7}}
                      />
                      <Image
                        source={require('../assets/icon/star_ic.png')}
                        style={{height: 8, width: 7}}
                      />
                      <Image
                        source={require('../assets/icon/empty_star_ic.png')}
                        style={{height: 8, width: 7}}
                      />
                    </View>
                    <Text
                      style={{
                        color: '#D8D8D8',
                        fontSize: 10,
                        fontWeight: '500',
                      }}>
                      4.7 ratings
                    </Text>
                  </View>
                  <View style={{width: '20%', alignItems: 'flex-end'}}>
                    <Image
                      source={require('../assets/icon/white_heart_ic.png')}
                      style={{height: 13, width: 12}}
                    />
                  </View>
                </View>
                <Text
                  style={{
                    color: '#201F26',
                    fontSize: 12,
                    fontWeight: '600',
                    marginVertical: 5,
                  }}>
                  Jack Repair Seturan
                </Text>
                <Text
                  style={{color: '#D8D8D8', fontSize: 9, fontWeight: '500'}}
                  numberOfLines={1}>
                  Jl. Seturan Kec. Laweyan . . .
                </Text>
                <View
                  style={{
                    borderRadius: 20,
                    width: 60,
                    backgroundColor: '#11A84E1F',
                    alignItems: 'center',
                    marginVertical: 5,
                  }}>
                  <Text
                    style={{
                      color: '#11A84E',
                      paddingHorizontal: 5,
                      paddingVertical: 5,
                      fontSize: 12,
                    }}>
                    Buka
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;
