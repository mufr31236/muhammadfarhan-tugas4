import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import React from 'react';
import CheckBox from '@react-native-community/checkbox';

const Form = ({navigation, route}) => {
  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 15,
            paddingVertical: 10,
            shadowOpacity: 1,
            shadowRadius: 4,
            shadowColor: 'black',
            elevation: 4,
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              style={{marginRight: 10}}
              source={require('../assets/icon/black_back_arrow_ic.png')}
            />
          </TouchableOpacity>

          <Text
            style={{
              color: '#201F26',
              fontSize: 18,
              fontWeight: '700',
              tintColor: 'black',
            }}>
            Formulir Pemesanan
          </Text>
        </View>
        <View style={{marginTop: 30, marginHorizontal: 20}}>
          <Text style={{color: '#BB2427', fontSize: 12, fontWeight: '600'}}>
            Merek
          </Text>
          <TextInput
            placeholder="Masukkan Merk barang"
            style={{
              marginTop: 15,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 15,
            }}>
            Warna
          </Text>
          <TextInput
            placeholder="Warna Barang, cth : Merah - Putih "
            style={{
              marginTop: 15,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 15,
            }}>
            Ukuran
          </Text>
          <TextInput
            placeholder="Cth : S, M, L / 39,40"
            style={{
              marginTop: 15,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 15,
            }}>
            Photo
          </Text>
          <View
            style={{
              borderRadius: 10,
              borderColor: '#BB2427',
              flexDirection: 'column',
              borderWidth: 1,
              paddingVertical: 20,
              alignItems: 'center',
              marginVertical: 20,
              height: 84,
              width: 84,
            }}>
            <Image
              source={require('../assets/icon/camera_ic.png')}
              style={{height: 24, width: 24}}
            />
            <Text style={{color: '#BB2427', fontWeight: '400', fontSize: 12}}>
              Add Photo
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <CheckBox disabled={false} />
            <Text
              style={{
                color: '#201F26',
                fontSize: 14,
                fontWeight: '600',
                marginTop: 5,
                marginLeft: 15,
              }}>
              Ganti Sol Sepatu
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <CheckBox disabled={false} />
            <Text
              style={{
                color: '#201F26',
                fontSize: 14,
                fontWeight: '600',
                marginTop: 5,
                marginLeft: 15,
              }}>
              Jahit Sepatu
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <CheckBox disabled={false} />
            <Text
              style={{
                color: '#201F26',
                fontSize: 14,
                fontWeight: '600',
                marginTop: 5,
                marginLeft: 15,
              }}>
              Repaint Sepatu
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <CheckBox disabled={false} />
            <Text
              style={{
                color: '#201F26',
                fontSize: 14,
                fontWeight: '400',
                marginTop: 5,
                marginLeft: 15,
              }}>
              Cuci Sepatu
            </Text>
          </View>
          <Text
            style={{
              color: '#BB2427',
              fontWeight: '600',
              fontSize: 12,
              marginTop: 20,
            }}>
            Catatan
          </Text>
          <View
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 10,
              marginVertical: 10,
              paddingHorizontal: 5,
            }}>
            <TextInput
              style={{textAlignVertical: 'top', height: 93}}
              placeholder="Cth : ingin ganti sol baru"
              multiline={true}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('keranjang');
          }}
          style={{
            backgroundColor: '#BB2427',
            alignSelf: 'center',
            width: '90%',
            padding: 15,
            alignItems: 'center',
            marginHorizontal: 10,
            borderRadius: 10,
            marginBottom: 30,
          }}>
          <Text style={{fontSize: 16, fontWeight: '700', color: '#FFFFFF'}}>
            Masukkan Keranjang
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Form;
