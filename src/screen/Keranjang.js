import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';

const Keranjang = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <View
        style={{
          backgroundColor: 'white',
          flexDirection: 'row',
          paddingHorizontal: 20,
          paddingVertical: 15,
          shadowOpacity: 1,
          shadowRadius: 4,
          shadowColor: 'black',
          elevation: 4,
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            style={{width: 24, height: 24, tintColor: 'black', marginRight: 5}}
            source={require('../assets/icon/back_arrow_ic.png')}
          />
        </TouchableOpacity>

        <Text style={{fontWeight: '700', color: '#201F26', fontSize: 18}}>
          Keranjang
        </Text>
      </View>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('summary');
        }}>
        <View
          style={{
            backgroundColor: 'white',
            marginVertical: 5,
            marginHorizontal: 5,
            flexDirection: 'row',
            borderRadius: 10,
            paddingVertical: 20,
            paddingHorizontal: 15,
            shadowOpacity: 1,
            shadowRadius: 4,
            shadowColor: 'black',
            elevation: 4,
          }}>
          <Image
            style={{height: 84, width: 84, marginRight: 10}}
            source={require('../assets/image/product_img.png')}
          />
          <View>
            <Text
              style={{
                color: '#000000',
                fontSize: 12,
                fontWeight: '500',
                marginVertical: 5,
              }}>
              New Balance - Pink Abu - 40
            </Text>
            <Text
              style={{
                color: '#737373',
                fontSize: 12,
                fontWeight: '500',
                marginVertical: 5,
              }}>
              Cuci Sepatu
            </Text>
            <Text
              style={{
                color: '#737373',
                fontSize: 12,
                fontWeight: '500',
                marginVertical: 5,
              }}>
              Note : -
            </Text>
          </View>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('form');
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop: 30,
          }}>
          <Image
            source={require('../assets/icon/add_product_ic.png')}
            style={{height: 24, width: 24, marginRight: 10}}
          />
          <Text style={{color: '#BB2427', fontSize: 14, fontWeight: '700'}}>
            Tambah Barang
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 45,
          backgroundColor: '#BB2427',
          alignSelf: 'center',
          width: '90%',
          padding: 15,
          alignItems: 'center',
          marginHorizontal: 10,
          borderRadius: 10,
        }}>
        <Text style={{fontSize: 16, fontWeight: '700', color: '#FFFFFF'}}>
          Selanjutnya
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Keranjang;
