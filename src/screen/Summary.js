import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';

const Summary = ({navigation, route}) => {
  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 15,
          paddingVertical: 15,
          shadowOpacity: 1,
          shadowRadius: 4,
          shadowColor: '#DCDCDC40',
          elevation: 4,
          marginBottom: 5,
          backgroundColor: '#FFFFFF',
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            style={{marginRight: 10}}
            source={require('../assets/icon/black_back_arrow_ic.png')}
          />
        </TouchableOpacity>

        <Text
          style={{
            color: '#201F26',
            fontSize: 18,
            fontWeight: '700',
            tintColor: 'black',
          }}>
          Summary
        </Text>
      </View>
      <View
        style={{
          backgroundColor: '#FFFFFF',
          paddingVertical: 10,
          paddingHorizontal: 20,
        }}>
        <Text
          style={{
            color: '#979797',
            fontSize: 14,
            fontWeight: '500',
            marginVertical: 5,
          }}>
          Data Customer
        </Text>
        <Text style={{color: '#201F26', marginVertical: 5}}>
          Agil Bani (0813763476)
        </Text>
        <Text style={{color: '#201F26', marginVertical: 5}}>
          Jl. Perumnas, Condong catur, Sleman, Yogyakarta
        </Text>
        <Text style={{color: '#201F26', marginVertical: 5}}>
          gantengdoang@dipanggang.com
        </Text>
      </View>
      <View
        style={{
          backgroundColor: '#FFFFFF',
          paddingVertical: 10,
          paddingHorizontal: 20,
        }}>
        <Text
          style={{
            color: '#979797',
            fontSize: 14,
            fontWeight: '500',
            marginVertical: 5,
          }}>
          Alamat Outlet Tujuan
        </Text>
        <Text style={{color: '#201F26', marginVertical: 5}}>
          Jack Repair - Seturan (027-343457)
        </Text>
        <Text style={{color: '#201F26', marginVertical: 5}}>
          Jl. Affandi No 18, Sleman, Yogyakarta
        </Text>
      </View>
      <View
        style={{
          backgroundColor: '#FFFFFF',
          paddingVertical: 10,
          paddingHorizontal: 20,
        }}>
        <Text
          style={{
            color: '#979797',
            fontSize: 14,
            fontWeight: '500',
            marginVertical: 5,
          }}>
          Barang
        </Text>
        <View
          style={{
            backgroundColor: 'white',
            flexDirection: 'row',
            borderRadius: 10,
          }}>
          <Image
            style={{height: 84, width: 84, marginRight: 10}}
            source={require('../assets/image/product_img.png')}
          />
          <View style={{marginLeft: 10}}>
            <Text
              style={{
                color: '#000000',
                fontSize: 12,
                fontWeight: '500',
                marginVertical: 5,
              }}>
              New Balance - Pink Abu - 40
            </Text>
            <Text
              style={{
                color: '#737373',
                fontSize: 12,
                fontWeight: '500',
                marginVertical: 5,
              }}>
              Cuci Sepatu
            </Text>
            <Text
              style={{
                color: '#737373',
                fontSize: 12,
                fontWeight: '500',
                marginVertical: 5,
              }}>
              Note : -
            </Text>
          </View>
        </View>
      </View>

      <TouchableOpacity
        onPress={() => {
          navigation.navigate('sukses');
        }}
        style={{
          position: 'absolute',
          bottom: 45,
          backgroundColor: '#BB2427',
          alignSelf: 'center',
          width: '90%',
          padding: 15,
          alignItems: 'center',
          marginHorizontal: 10,
          borderRadius: 10,
        }}>
        <Text style={{fontSize: 16, fontWeight: '700', color: '#FFFFFF'}}>
          Reservasi Sekarang
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Summary;
