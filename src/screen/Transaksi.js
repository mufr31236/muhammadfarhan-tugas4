import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';

const Transaksi = ({navigation, route}) => {
  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 15,
          paddingVertical: 15,
          shadowOpacity: 1,
          shadowRadius: 4,
          shadowColor: '#DCDCDC40',
          elevation: 4,
          marginBottom: 5,
          backgroundColor: '#FFFFFF',
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            style={{marginRight: 10}}
            source={require('../assets/icon/black_back_arrow_ic.png')}
          />
        </TouchableOpacity>

        <Text
          style={{
            color: '#201F26',
            fontSize: 18,
            fontWeight: '700',
            tintColor: 'black',
          }}>
          Transaksi
        </Text>
      </View>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('transactionnavigation', {screen: 'detail'});
        }}>
        <View
          style={{
            backgroundColor: 'white',
            marginVertical: 5,
            marginHorizontal: 10,
            flexDirection: 'column',
            borderRadius: 10,
            paddingVertical: 20,
            paddingHorizontal: 15,
            shadowOpacity: 1,
            shadowRadius: 4,
            shadowColor: 'black',
            elevation: 4,
          }}>
          <Text
            style={{
              color: '#BDBDBD',
              fontSize: 12,
              fontWeight: '500',
              marginBottom: 10,
            }}>
            20 Desember 2020 09:00
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 12,
              fontWeight: '500',
              marginVertical: 5,
            }}>
            New Balance - Pink Abu - 40
          </Text>
          <Text style={{color: '#201F26', fontWeight: '400', fontSize: 12}}>
            Cuci Sepatu
          </Text>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 10,
            }}>
            <Text style={{color: '#201F26', fontSize: 12, fontWeight: '400'}}>
              Kode Reservasi :
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 12,
                  fontWeight: '700',
                }}>
                {'   '}CS201201
              </Text>
            </Text>
            <View
              style={{
                flex: 1,
              }}>
              <View
                style={{
                  backgroundColor: '#F29C1F29',
                  borderRadius: 15,
                  paddingHorizontal: 10,
                  alignSelf: 'flex-end',
                }}>
                <Text
                  style={{color: '#FFC107', fontWeight: '400', fontSize: 12}}>
                  Reserved
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Transaksi;
