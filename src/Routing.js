import {View, Text} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import SplashScreen from './screen/SplashScreen';
import AuthNavigation from './AuthNavigation';
import ProfileNavigation from './ProfileNavigation';
import HomeNavigation from './HomeNavigation';
import TabNavigation from './TabNavigation';
import TransactionNavigation from './TransactionNavigation';

const Stack = createNativeStackNavigator();

const Routing = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="splash" component={SplashScreen} />
        <Stack.Screen name="authnavigation" component={AuthNavigation} />
        <Stack.Screen name="profilenavigation" component={ProfileNavigation} />
        <Stack.Screen name="homenavigation" component={HomeNavigation} />
        <Stack.Screen
          name="transactionnavigation"
          component={TransactionNavigation}
        />
        <Stack.Screen name="tabnavigation" component={TabNavigation} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routing;
